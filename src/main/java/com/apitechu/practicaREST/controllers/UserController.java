package com.apitechu.practicaREST.controllers;

import com.apitechu.practicaREST.models.UserModel;
import com.apitechu.practicaREST.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.DelegatingServerHttpResponse;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.List;
// atajos de teclado
// sout te saca directo el System.out.println();
// psvm --> public static void main ...

@RestController
@RequestMapping("/apitechuREST/v1")

public class UserController {

    // El Controller necesita estar cableado con el Servicio
    @Autowired
    UserService userService;

    @GetMapping("/users")
    // public ResponseEntity< List<UserModel>> getAllUsers(@RequestParam Optional<String> sorted){
    // en el rest hay que poner $orderby en vez de sorted para seguir el standard ver doc odata https://www.odata.org/documentation/
    // si pones $ en la url te lo pedirá tb en postman, es mas limpiio de esta forma porque asignas tu y no por defecto
    public ResponseEntity< List<UserModel>> getAllUsers(@RequestParam (name = "sorted",required = false, defaultValue = "") String sorted_value){
        System.out.println("get All Users sorted by " +  (sorted_value.isBlank()? "none" : sorted_value));
        return new ResponseEntity<List<UserModel>>(this.userService.findAll(sorted_value), HttpStatus.OK);
    }


    @GetMapping("/users/{id}")
    // Obterner un usuario en base a su ID
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("get User by ID");
        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado" ,
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    // CREar un usuario en base a un body por peti POST
    @PostMapping("/users")
    public ResponseEntity<UserModel> addProduct(@RequestBody UserModel user){
        System.out.println("create User by POST request");
        return new ResponseEntity<>(this.userService.addUser(user),HttpStatus.CREATED);
    }

    // Actualizar completamente un usuario (menos la id) en base a su id.
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user ,@PathVariable String id) {
        System.out.println("update user");

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if ( userToUpdate.isPresent() ) {
            System.out.println("Usuario encontrado ,se puede actualizar");
            this.userService.update(user);
        }

        return new ResponseEntity<UserModel>(user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping ("/users/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("Delete usuario");
        Boolean deletedProduct = this.userService.deleteId(id);

        return new ResponseEntity<String>(
                deletedProduct ? "Usuario Borrado" : "Usuario NO borrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
