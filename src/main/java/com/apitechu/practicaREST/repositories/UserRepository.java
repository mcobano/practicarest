package com.apitechu.practicaREST.repositories;

import com.apitechu.practicaREST.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository <UserModel,String> {
}
