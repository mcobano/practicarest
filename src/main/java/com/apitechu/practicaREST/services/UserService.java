package com.apitechu.practicaREST.services;

import com.apitechu.practicaREST.models.UserModel;
import com.apitechu.practicaREST.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.server.DelegatingServerHttpResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// Servicion necesario para montar el Controller


@Service
public class UserService {

    // El servicio necesita del repositorio por eso se cablea
    @Autowired
    UserRepository userRepository;

    public Optional<UserModel> findById (String id) {
        System.out.println("Find by ID prodcut Service");
        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel user){
        System.out.println("add user en Service");
        // Podemos  usar esta return (this.productRepository.save(product));
        // o Pedemos usar el insert tb
        return (this.userRepository.insert(user));
    }

    public UserModel update (UserModel user){
        System.out.println("Update (PUT) by ID user Service");
        return (this.userRepository.save(user));
    }

    public boolean deleteId (String id) {
        System.out.println("Delete by ID prodcut Service");
        boolean result = false;

        if (this.findById(id).isPresent()) {
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }

    public List<UserModel> findAll (String fieldId){
        System.out.println("Findlall en product service");
        if ( fieldId.isBlank() == true)
            return this.userRepository.findAll();
        else
            return this.userRepository.findAll(Sort.by( Sort.Direction.DESC,fieldId));
    }

}
